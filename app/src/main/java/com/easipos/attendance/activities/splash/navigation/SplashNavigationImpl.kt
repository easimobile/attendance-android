package com.easipos.attendance.activities.splash.navigation

import android.app.Activity
import com.easipos.attendance.activities.main.MainActivity

class SplashNavigationImpl : SplashNavigation {

    override fun navigateToMain(activity: Activity) {
        activity.startActivity(MainActivity.newIntent(activity))
        activity.finishAffinity()
    }
}