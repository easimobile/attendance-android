package com.easipos.attendance.activities.splash.navigation

import android.app.Activity

interface SplashNavigation {

    fun navigateToMain(activity: Activity)
}