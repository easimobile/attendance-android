package com.easipos.attendance.activities.splash.mvp

import com.easipos.attendance.base.View

interface SplashView : View {

    fun showUpdateAppDialog()

    fun navigateToLogin()

    fun navigateToMain()
}
