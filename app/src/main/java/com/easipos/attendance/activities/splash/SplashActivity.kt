package com.easipos.attendance.activities.splash

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import com.easipos.attendance.R
import com.easipos.attendance.activities.splash.mvp.SplashPresenter
import com.easipos.attendance.activities.splash.mvp.SplashView
import com.easipos.attendance.activities.splash.navigation.SplashNavigation
import com.easipos.attendance.base.CustomBaseAppCompatActivity
import com.easipos.attendance.bundle.ParcelData
import com.easipos.attendance.room.RoomService
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.delay
import io.github.anderscheow.library.kotlinExt.rate
import org.kodein.di.generic.instance

class SplashActivity : CustomBaseAppCompatActivity(), SplashView {

    companion object {
        fun newIntent(context: Context, clearDb: Boolean = false): Intent {
            return Intent(context, SplashActivity::class.java).apply {
                this.putExtra(ParcelData.CLEAR_DB, clearDb)
            }
        }
    }

    //region Variables
    private val navigation by instance<SplashNavigation>()
    private val roomService by instance<RoomService>()

    private val presenter by lazy { SplashPresenter(application) }

    private val clearDb by argument(ParcelData.CLEAR_DB, false)
    //endregion

    //region Lifecycle
    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_splash

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        if (clearDb) {
            AsyncTask.execute {
                roomService.runInTransaction {
                    roomService.clearAllTables()
                }
            }
        }

//        checkVersion()

        delay(1000) {
            navigation.navigateToMain(this)
        }
    }
    //endregion

    //region SplashView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
    }

    override fun showUpdateAppDialog() {
        showYesAlertDialog(getString(R.string.prompt_update_app), buttonText = R.string.action_upgrade_now) {
            this@SplashActivity.rate()
            finishAffinity()
        }
    }

    override fun navigateToLogin() {
    }

    override fun navigateToMain() {
    }
    //endregion

    //region Action Methods
    private fun checkVersion() {
        presenter.checkVersion()
    }
    //endregion
}
