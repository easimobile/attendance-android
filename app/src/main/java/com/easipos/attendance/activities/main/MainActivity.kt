package com.easipos.attendance.activities.main

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import com.easipos.attendance.R
import com.easipos.attendance.activities.main.mvp.MainPresenter
import com.easipos.attendance.activities.main.mvp.MainView
import com.easipos.attendance.activities.main.navigation.MainNavigation
import com.easipos.attendance.api.requests.attendance.AttendanceInfoRequestModel
import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.base.CustomBaseAppCompatActivity
import com.easipos.attendance.tools.Preference
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import kotlinx.android.synthetic.main.activity_main.*
import org.kodein.di.generic.instance
import java.util.*

class MainActivity : CustomBaseAppCompatActivity(), MainView {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()
    private val validator by instance<Validator>()

    private val presenter by lazy { MainPresenter(application) }

    private val fusedLocationClient by lazy { LocationServices.getFusedLocationProviderClient(this) }
    private val locationCallback by lazy { object : LocationCallback() {} }
    private val locationRequest by lazy {
        LocationRequest.create().apply {
            this.interval = 60000
            this.fastestInterval = 5000
            this.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }
    //endregion

    //region Lifecycle
    @SuppressLint("MissingPermission")
    override fun onResume() {
        super.onResume()
        fusedLocationClient.requestLocationUpdates(locationRequest,
            locationCallback,
            Looper.getMainLooper())
    }

    override fun onPause() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
        super.onPause()
    }

    override fun onDestroy() {
        presenter.onDetachView()
        super.onDestroy()
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)
        presenter.onAttachView(this)

        setupViews()
        setupListeners()
    }
    //endregion

    //region MainView Abstract Methods
    override fun toastMessage(message: CharSequence) {
    }

    override fun toastMessage(message: Int) {
    }

    override fun setLoadingIndicator(active: Boolean, message: Int) {
        checkLoadingIndicator(active, message)
    }

    override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
        showYesAlertDialog(message, getString(R.string.title_attendance), R.string.action_ok, action = action)
    }

    override fun clearViews() {
        text_input_edit_text_reason.text = null
        auto_complete_action.text = null
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_input_edit_text_username.setText(Preference.prefUsername)
        text_input_edit_text_password.setText(Preference.prefPassword)

        (text_input_layout_action.editText as? AutoCompleteTextView)?.apply {
            val actions = resources.getStringArray(R.array.label_action_array).toList()
            val adapter = ArrayAdapter(this@MainActivity, android.R.layout.simple_list_item_1, actions)
            this.setAdapter(adapter)
            this.inputType = EditorInfo.TYPE_NULL
        }
    }

    private fun setupListeners() {
        button_submit.click {
            attemptInsertAttendance()
        }
    }

    private fun attemptInsertAttendance() {
        val usernameValidation = text_input_layout_username.validate()
            .notBlank(R.string.error_field_required)
        val passwordValidation = text_input_layout_password.validate()
            .notBlank(R.string.error_field_required)
        val reasonValidation = text_input_layout_reason.validate()
            .notBlank(R.string.error_field_required)
        val actionValidation = text_input_layout_action.validate()
            .notBlank(R.string.error_field_required)

        validator.setListener(object : Validator.OnValidateListener {
                override fun onValidateFailed(errors: List<String>) {
                }

                override fun onValidateSuccess(values: List<String>) {
                    val username = text_input_edit_text_username.text.toString().trim()
                    val password = text_input_edit_text_password.text.toString().trim()
                    val reason = text_input_edit_text_reason.text.toString().trim()
                    val action = auto_complete_action.text.toString().trim().toUpperCase(Locale.getDefault())

                    attemptGetLocation(username, password, reason, action)
                }
            }).validate(usernameValidation, passwordValidation, reasonValidation, actionValidation)
    }

    private fun attemptGetLocation(username: String, password: String, reason: String, action: String) {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                    token?.continuePermissionRequest()
                }

                override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                    report?.let {
                        if (report.deniedPermissionResponses.isNotEmpty()) {
                            showErrorAlertDialog(getString(R.string.error_permission_location_required))
                        } else {
                            getLocation(username, password, reason, action)
                        }
                    }
                }
            }).check()
    }

    @SuppressLint("MissingPermission")
    private fun getLocation(username: String, password: String, reason: String, action: String) {
        setLoadingIndicator(true)
        fusedLocationClient.locationAvailability.apply {
            this.addOnSuccessListener { locationAvailability ->
                if (locationAvailability.isLocationAvailable) {
                    fusedLocationClient.lastLocation.apply {
                        this.addOnSuccessListener { location ->
                            location?.let {
                                insertAttendance(username, password, reason, action, location)
                            } ?: run {
                                setLoadingIndicator(false)
                                showErrorAlertDialog(getString(R.string.error_location_not_available))
                            }
                        }
                        this.addOnFailureListener {
                            it.printStackTrace()
                            setLoadingIndicator(false)
                            showErrorAlertDialog(getString(R.string.error_failed_get_location))
                        }
                    }
                } else {
                    setLoadingIndicator(false)
                    showErrorAlertDialog(getString(R.string.error_failed_get_location))
                }
            }
            this.addOnFailureListener {
                it.printStackTrace()
                setLoadingIndicator(false)
                showErrorAlertDialog(getString(R.string.error_failed_get_location))
            }
        }
    }

    private fun insertAttendance(username: String, password: String, reason: String, action: String, location: Location) {
        val model = AttendanceRequestModel(
            attendanceWebRequest = AttendanceInfoRequestModel(
                employeeCode = username,
                password = password,
                status = action,
                type = "Android - ${location.latitude},${location.longitude}",
                reason = reason
            )
        )

        presenter.doInsertAttendance(this, model)
    }
    //endregion
}
