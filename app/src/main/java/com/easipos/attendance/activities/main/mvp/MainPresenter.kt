package com.easipos.attendance.activities.main.mvp

import android.app.Application
import android.content.Context
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.easipos.attendance.R
import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.base.Presenter
import com.easipos.attendance.models.Result
import com.easipos.attendance.repositories.attendance.AttendanceRepository
import com.easipos.attendance.tools.ClockOutNotificationWorker
import com.easipos.attendance.tools.Preference
import com.easipos.attendance.util.ErrorUtil
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance
import java.util.concurrent.TimeUnit

class MainPresenter(application: Application)
    : Presenter<MainView>(application) {

    private val attendanceRepository by instance<AttendanceRepository>()

    fun doInsertAttendance(context: Context, model: AttendanceRequestModel) {
        view?.setLoadingIndicator(true)
        launch {
            val result = attendanceRepository.insertAttendance(model)
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.EmptySuccess -> {
                        Preference.prefUsername = model.attendanceWebRequest.employeeCode
                        Preference.prefPassword = model.attendanceWebRequest.password

                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(application.getString(R.string.prompt_submit_successful)) {
                            view?.clearViews()
                        }

                        scheduleNotification(context)
                    }

                    is Result.Error -> {
                        view?.setLoadingIndicator(false)
                        view?.showErrorAlertDialog(ErrorUtil.parseException(result.exception))
                    }

                    else -> {
                    }
                }
            }
        }
    }

    private fun scheduleNotification(context: Context) {
        val notificationWork = OneTimeWorkRequest.Builder(ClockOutNotificationWorker::class.java)
            .setInitialDelay(9, TimeUnit.HOURS).build()

        val instanceWorkManager = WorkManager.getInstance(context)
        instanceWorkManager.beginUniqueWork(ClockOutNotificationWorker.WORKER_NAME,
            ExistingWorkPolicy.REPLACE, notificationWork).enqueue()
    }
}
