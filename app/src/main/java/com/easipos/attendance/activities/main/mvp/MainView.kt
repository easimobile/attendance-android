package com.easipos.attendance.activities.main.mvp

import com.easipos.attendance.base.View

interface MainView : View {

    fun clearViews()
}
