package com.easipos.attendance.activities.splash.mvp

import android.app.Application
import com.easipos.attendance.api.requests.precheck.CheckVersionRequestModel
import com.easipos.attendance.base.Presenter
import com.easipos.attendance.managers.UserManager
import com.easipos.attendance.models.Result
import com.easipos.attendance.repositories.precheck.PrecheckRepository
import com.easipos.attendance.tools.Preference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.kodein.di.generic.instance

class SplashPresenter(application: Application)
    : Presenter<SplashView>(application) {

    private val precheckRepository by instance<PrecheckRepository>()

    fun checkVersion() {
        launch {
            val result = precheckRepository.checkVersion(CheckVersionRequestModel())
            withContext(Dispatchers.Main) {
                when (result) {
                    is Result.Success<Boolean> -> {
                        if (result.data) {
                            view?.showUpdateAppDialog()
                        } else {
                            checkIsAuthenticated()
                        }
                    }

                    is Result.Error -> {
                        checkIsAuthenticated()
                    }

                    else -> {
                    }
                }
            }
        }
    }

    fun checkIsAuthenticated() {
        if (Preference.prefIsLoggedIn && UserManager.token != null) {
            view?.navigateToMain()
        } else {
            view?.navigateToLogin()
        }
    }
}

