package com.easipos.attendance.di.modules

import com.easipos.attendance.activities.main.navigation.MainNavigation
import com.easipos.attendance.activities.main.navigation.MainNavigationImpl
import com.easipos.attendance.activities.splash.navigation.SplashNavigation
import com.easipos.attendance.activities.splash.navigation.SplashNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<SplashNavigation>() with provider { SplashNavigationImpl() }
            bind<MainNavigation>() with provider { MainNavigationImpl() }
        }
    }
}
