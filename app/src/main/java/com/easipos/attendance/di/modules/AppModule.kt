package com.easipos.attendance.di.modules

import android.app.Application
import androidx.room.Room
import com.easipos.attendance.Easi
import com.easipos.attendance.api.misc.AuthInterceptor
import com.easipos.attendance.api.misc.AuthOkhttpClient
import com.easipos.attendance.api.misc.TokenAuthenticator
import com.easipos.attendance.api.services.Api
import com.easipos.attendance.datasource.DataFactory
import com.easipos.attendance.repositories.attendance.AttendanceDataRepository
import com.easipos.attendance.repositories.attendance.AttendanceRepository
import com.easipos.attendance.repositories.precheck.PrecheckDataRepository
import com.easipos.attendance.repositories.precheck.PrecheckRepository
import com.easipos.attendance.room.RoomService
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import io.github.anderscheow.library.di.modules.BaseModule
import io.github.anderscheow.library.di.modules.CommonBaseModule
import io.github.anderscheow.validator.Validator
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.eagerSingleton
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class CommonModule(private val easi: Easi) : CommonBaseModule() {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        super.provideAdditionalModule(builder)
        builder.apply {
            bind<Easi>() with singleton { instance<Application>() as Easi }
            bind<Validator>() with singleton { Validator.with(easi) }

            bind<DataFactory>() with singleton {
                DataFactory(instance(), instance(), instance(), instance(), instance())
            }

            bind<PrecheckRepository>() with singleton { PrecheckDataRepository(instance()) }
            bind<AttendanceRepository>() with singleton { AttendanceDataRepository(instance()) }
        }
    }
}

class ApiModule(private val userAgent: String,
                private val endpoint: String,
                private val authorisation: String
) : BaseModule("apiModule") {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<AuthInterceptor>() with singleton { AuthInterceptor(userAgent, authorisation) }
            bind<TokenAuthenticator>() with singleton { TokenAuthenticator(instance()) }
            bind<AuthOkhttpClient>() with singleton { AuthOkhttpClient(instance(), instance()) }
            bind<RxJava2CallAdapterFactory>() with singleton { RxJava2CallAdapterFactory.create() }
            bind<GsonConverterFactory>() with singleton {
                val gson = GsonBuilder()
                    .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                    .create()
                GsonConverterFactory.create(gson)
            }
            bind<Api>() with singleton {
                Retrofit.Builder()
                    .baseUrl(endpoint)
                    .client(instance<AuthOkhttpClient>().getAuthOkhttpClient())
                    .addCallAdapterFactory(instance())
                    .addConverterFactory(instance())
                    .build()
                    .create(Api::class.java)
            }
        }
    }
}

class DatabaseModule(private val easi: Easi,
                     private val dbName: String
) : BaseModule("databaseModule") {
    override fun provideAdditionalModule(builder: Kodein.Builder) {
        builder.apply {
            bind<RoomService>() with eagerSingleton {
                Room.databaseBuilder(easi.applicationContext,
                    RoomService::class.java, dbName)
                    .fallbackToDestructiveMigration()
                    .build()
            }
        }
    }
}