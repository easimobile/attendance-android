package com.easipos.attendance.models

data class Auth(val token: String)