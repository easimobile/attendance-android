package com.easipos.attendance.tools

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Color
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.media.RingtoneManager.getDefaultUri
import android.os.Build
import android.os.PowerManager
import androidx.core.app.NotificationCompat
import androidx.work.ListenableWorker.Result.success
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.easipos.attendance.R
import com.easipos.attendance.activities.main.MainActivity

class ClockOutNotificationWorker(private val context: Context, params: WorkerParameters) : Worker(context, params) {
    companion object {
        const val WORKER_NAME = "CLOCK_OUT_NOTIFICATION"
        private const val NOTIFICATION_ID = "notification-id"
        private const val NOTIFICATION_CHANNEL_ID = "10001"
        private const val NOTIFICATION_CHANNEL_NAME = "Default Channel"
    }

    override fun doWork(): Result {
        sendNotification()
        return success()
    }

    private fun sendNotification() {
        val notificationId = Preference.prefNotificationId
        val intent = Intent(context, MainActivity::class.java).apply {
            this.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            this.putExtra(NOTIFICATION_ID, notificationId)
        }

        val notificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        val titleNotification = "Clock Out Reminder"
        val subtitleNotification = "Please remember to clock out."
        val pendingIntent = getActivity(context, 0, intent, 0)
        val notification = NotificationCompat.Builder(
            context,
            NOTIFICATION_CHANNEL_ID
        ).apply {
            this.setSmallIcon(R.mipmap.ic_launcher)
            this.setContentTitle(titleNotification).setContentText(subtitleNotification)
            this.setDefaults(NotificationCompat.DEFAULT_ALL)
            this.setContentIntent(pendingIntent)
            this.setAutoCancel(true)
            this.priority = NotificationCompat.PRIORITY_MAX
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notification.setChannelId(NOTIFICATION_CHANNEL_ID)

            val ringtoneManager = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val audioAttributes = AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()

            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_ID,
                NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                this.enableLights(true)
                this.lightColor = Color.RED
                this.enableVibration(true)
                this.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                this.setSound(ringtoneManager, audioAttributes)
            }
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(notificationId, notification.build())
        Preference.prefNotificationId = notificationId + 1

        wakeUpScreen()
    }

    private fun wakeUpScreen() {
        val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
        val isScreenOn = pm.isInteractive
        if (isScreenOn.not()) {
            val wl = pm.newWakeLock(
                PowerManager.SCREEN_DIM_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP,
                "myApp:notificationLock"
            )
            wl.acquire(100)
        }
    }
}