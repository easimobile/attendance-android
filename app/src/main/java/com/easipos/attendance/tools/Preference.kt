package com.easipos.attendance.tools

import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_USERNAME = "PREF_USERNAME"
    private const val PREF_PASSWORD = "PREF_PASSWORD"
    private const val PREF_NOTIFICATION_ID = "PREF_NOTIFICATION_ID"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefUsername: String
        get() = Prefs.getString(PREF_USERNAME, "")
        set(username) = Prefs.putString(PREF_USERNAME, username)

    var prefPassword: String
        get() = Prefs.getString(PREF_PASSWORD, "")
        set(password) = Prefs.putString(PREF_PASSWORD, password)

    var prefNotificationId: Int
        get() = Prefs.getInt(PREF_NOTIFICATION_ID, 1)
        set(notificationId) = Prefs.putInt(PREF_NOTIFICATION_ID, notificationId)

    fun logout() {
        prefIsLoggedIn = false

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}