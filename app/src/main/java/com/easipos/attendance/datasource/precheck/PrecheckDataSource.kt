package com.easipos.attendance.datasource.precheck

import com.easipos.attendance.api.misc.parseException
import com.easipos.attendance.api.misc.parseResponse
import com.easipos.attendance.api.requests.precheck.CheckVersionRequestModel
import com.easipos.attendance.api.services.Api
import com.easipos.attendance.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PrecheckDataSource(private val api: Api) : PrecheckDataStore {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.checkVersion(model.toFormDataBuilder().build())
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
