package com.easipos.attendance.datasource.precheck

import com.easipos.attendance.api.requests.precheck.CheckVersionRequestModel
import com.easipos.attendance.models.Result

interface PrecheckDataStore {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
