package com.easipos.attendance.datasource.attendance

import com.easipos.attendance.api.misc.parseException
import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.api.services.Api
import com.easipos.attendance.models.Result
import com.easipos.attendance.util.ApiErrorException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class AttendanceDataSource(private val api: Api) : AttendanceDataStore {

    override suspend fun insertAttendance(model: AttendanceRequestModel): Result<Nothing> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.insertAttendance(model)["AttendanceGetEmployeeResponse"] as Map<*, *>
                val status = response["Status"] as String
                if (status == "0") {
                    val errorResponse = response["errorResponse"] as Map<*, *>
                    val code = (errorResponse["Code"] as String).toIntOrNull() ?: -1
                    val message = errorResponse["Message"] as String
                    Result.Error(ApiErrorException(code, message))
                } else {
                    Result.EmptySuccess
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
