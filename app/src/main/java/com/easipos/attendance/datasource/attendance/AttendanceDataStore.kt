package com.easipos.attendance.datasource.attendance

import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.models.Result

interface AttendanceDataStore {

    suspend fun insertAttendance(model: AttendanceRequestModel): Result<Nothing>
}
