package com.easipos.attendance.datasource

import android.app.Application
import com.easipos.attendance.api.services.Api
import com.easipos.attendance.datasource.attendance.AttendanceDataSource
import com.easipos.attendance.datasource.attendance.AttendanceDataStore
import com.easipos.attendance.datasource.precheck.PrecheckDataSource
import com.easipos.attendance.datasource.precheck.PrecheckDataStore
import com.easipos.attendance.room.RoomService
import io.github.anderscheow.library.executor.PostExecutionThread
import io.github.anderscheow.library.executor.ThreadExecutor

class DataFactory(private val application: Application,
                  private val api: Api,
                  private val roomService: RoomService,
                  private val threadExecutor: ThreadExecutor,
                  private val postExecutionThread: PostExecutionThread
) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api)

    fun createAttendanceDataSource(): AttendanceDataStore =
        AttendanceDataSource(api)
}
