package com.easipos.attendance.repositories.precheck

import com.easipos.attendance.api.requests.precheck.CheckVersionRequestModel
import com.easipos.attendance.datasource.DataFactory
import com.easipos.attendance.models.Result

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
