package com.easipos.attendance.repositories.attendance

import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.models.Result

interface AttendanceRepository {

    suspend fun insertAttendance(model: AttendanceRequestModel): Result<Nothing>
}
