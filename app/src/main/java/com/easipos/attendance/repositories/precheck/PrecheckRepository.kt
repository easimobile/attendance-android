package com.easipos.attendance.repositories.precheck

import com.easipos.attendance.api.requests.precheck.CheckVersionRequestModel
import com.easipos.attendance.models.Result

interface PrecheckRepository {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
