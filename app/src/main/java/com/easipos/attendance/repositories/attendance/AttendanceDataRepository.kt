package com.easipos.attendance.repositories.attendance

import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import com.easipos.attendance.datasource.DataFactory
import com.easipos.attendance.models.Result

class AttendanceDataRepository(private val dataFactory: DataFactory) : AttendanceRepository {

    override suspend fun insertAttendance(model: AttendanceRequestModel): Result<Nothing> =
        dataFactory.createAttendanceDataSource()
            .insertAttendance(model)
}
