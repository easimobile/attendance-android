package com.easipos.attendance.api.misc

import com.easipos.attendance.models.Result
import com.easipos.attendance.util.ApiErrorException
import com.easipos.attendance.util.ErrorUtil
import java.net.ConnectException
import java.net.NoRouteToHostException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

fun <R, T> parseResponse(responseModel: ResponseModel<R>, transformer: (R) -> T): Result<T> {
    return if (responseModel.error.not()) {
        if (responseModel.data != null) {
            Result.Success(transformer(responseModel.data))
        } else {
            Result.Error(ErrorUtil.GENERIC_API_ERROR_EXCEPTION)
        }
    } else {
        Result.Error(ApiErrorException(responseModel.code, responseModel.message))
    }
}

fun parseResponse(responseModel: EmptyResponseModel): Result<Nothing> {
    return if (responseModel.error.not()) {
        Result.EmptySuccess
    } else {
        Result.Error(ApiErrorException(responseModel.code, responseModel.message))
    }
}

fun parseException(ex: Exception): Result<Nothing> {
    return when (ex) {
//        is HttpException -> {
//            try {
//                if (ex.response() != null && ex.response()!!.errorBody() != null) {
//                    val jsonObject = JSONObject(ex.response()!!.errorBody()!!.string())
//                    val response = jsonObject["AttendanceGetEmployeeResponse"] as JSONObject
//                    val errorResponse = response["errorResponse"] as JSONObject
//                    val code = (errorResponse["Code"] as String).toIntOrNull() ?: -1
//                    val message = errorResponse["Message"] as String
//                    Result.Error(ApiErrorException(code, message))
//                } else {
//                    Result.Error(ErrorUtil.GENERIC_API_ERROR_EXCEPTION)
//                }
//            } catch (ex: Exception) {
//                Result.Error(ex)
//            }
//        }
        is ConnectException, is TimeoutException, is SocketTimeoutException, is UnknownHostException, is NoRouteToHostException -> {
            Result.Error(ErrorUtil.TIMEOUT_ERROR_EXCEPTION)
        }
        else -> {
            Result.Error(ex)
        }
    }
}