package com.easipos.attendance.api

object ApiEndpoint {

    const val CHECK_VERSION = "ddcard/version/check"

    const val INSERT_ATTENDANCE = "InsertWebAttendance"
}
