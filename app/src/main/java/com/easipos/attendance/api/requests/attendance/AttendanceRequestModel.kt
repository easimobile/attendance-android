package com.easipos.attendance.api.requests.attendance

import com.google.gson.annotations.SerializedName

data class AttendanceRequestModel(
    @SerializedName("attendanceWebRequest")
    val attendanceWebRequest: AttendanceInfoRequestModel
)

data class AttendanceInfoRequestModel(
    @SerializedName("employeeCode")
    val employeeCode: String,

    @SerializedName("password")
    val password: String,

    @SerializedName("status")
    val status: String,

    @SerializedName("type")
    val type: String,

    @SerializedName("reason")
    val reason: String
)