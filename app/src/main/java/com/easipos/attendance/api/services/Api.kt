package com.easipos.attendance.api.services

import com.easipos.attendance.api.ApiEndpoint
import com.easipos.attendance.api.misc.ResponseModel
import com.easipos.attendance.api.requests.attendance.AttendanceRequestModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    fun checkVersion(@Body body: RequestBody): ResponseModel<Boolean>

    @POST(ApiEndpoint.INSERT_ATTENDANCE)
    suspend fun insertAttendance(@Body body: AttendanceRequestModel): Map<String, Any>
}
