package com.easipos.attendance.event_bus

data class NotificationCount(val count: Int)